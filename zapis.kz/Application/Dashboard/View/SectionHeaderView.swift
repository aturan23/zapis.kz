//
//  SectionHeaderView.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class SectionHeaderView: UIView {
    
    private var titleLabel = UILabel()
    
    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(type: SectionType) {
        titleLabel.text = type.rawValue
    }
}

extension SectionHeaderView {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    
    private func addSubviews() {
        addSubview(titleLabel)
    }
    
    private func addConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.top.equalTo(8)
            make.bottom.equalTo(-8)
        }
    }
    
    private func stylizeViews() {
        titleLabel.textColor = .black
        titleLabel.font = .systemFont(ofSize: 17)
        
        backgroundColor = .clear
    }
}
