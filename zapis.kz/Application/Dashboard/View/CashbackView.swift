//
//  CashbackView.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class CashbackView: UIView {
    
   private var titleLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(text: String) {
        titleLabel.text = text
    }
}

extension CashbackView {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    
    private func addSubviews() {
        addSubview(titleLabel)
    }
    
    private func addConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.centerY.centerX.equalToSuperview()
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .orange
        
        titleLabel.font = .systemFont(ofSize: 11)
        titleLabel.textColor = .white
        
        layer.cornerRadius = 8
        layer.masksToBounds = true
    }
}
