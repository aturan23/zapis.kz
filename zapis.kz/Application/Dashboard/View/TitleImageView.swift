//
//  TitleImageView.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

enum ViewSize {
    case large
    case small
}

class TitleImageView: UIView {
    
    private var viewSize: ViewSize
    private var titleLabel = UILabel()
    private var imageView = UIImageView()
    private var stackView = UIStackView()
    
    
    init(size: ViewSize = .small) {
        self.viewSize = size
        super.init(frame: .zero)
        setupViews()
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(text: String, sfName: String, color: UIColor = .black) {
        titleLabel.text = text
        imageView.image = UIImage(systemName: sfName)
        titleLabel.textColor = color
        imageView.tintColor = color
    }
}

extension TitleImageView {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    
    private func addSubviews() {
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(imageView)
        addSubview(stackView)
    }
    
    private func addConstraints() {
        imageView.snp.makeConstraints { (make) in
            make.height.width.equalTo(viewSize == .small ? 12 : 18)
        }
        stackView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    private func stylizeViews() {
        titleLabel.textColor = .black
        titleLabel.font = .systemFont(ofSize: viewSize == .small ? 12 : 18)
        
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = 4
    }
}
