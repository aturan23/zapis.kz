//
//  HorizontalListItemCVCell.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit
import Kingfisher

class HorizontalListItemCVCell: UICollectionViewCell {
    
    private let cashBackView = CashbackView()
    private let imageView = UIImageView()
    private let titleLabel = UILabel()
    private let typeLabel = UILabel()
    private let addressLabel = UILabel()
    private let lineView = UIView()
    private let ratingView = TitleImageView()
    private let reservedView = TitleImageView()
    
    func set(model: Firm) {
        if let url = model.pictureUrl, let path = url.imageUrl {
            imageView.kf.setImage(with: path)
        }
        titleLabel.text = model.name
        typeLabel.text = model.type
        addressLabel.text = model.address.withoutHtml
        if let cashback = model.prepaymentCashbackAmount {
            cashBackView.isHidden = false
            cashBackView.configure(text: cashback)
        }
        ratingView.configure(text: (model.averageRating ?? 0).description, sfName: "star.fill", color: .orange)
        reservedView.configure(text: (model.todayReservationsCount ?? 0).description, sfName: "person.2.fill")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HorizontalListItemCVCell {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    private func addSubviews() {
        addSubviews([imageView, cashBackView, typeLabel, titleLabel, addressLabel, lineView, reservedView, ratingView])
    }
    
    private func addConstraints() {
        imageView.snp.makeConstraints { (make) in
            make.width.top.equalToSuperview()
            make.height.equalToSuperview().multipliedBy(0.55)
        }
        cashBackView.snp.makeConstraints { (make) in
            make.right.equalTo(-8)
            make.height.equalTo(20)
            make.width.equalTo(40)
            make.bottom.equalTo(imageView.snp.bottom).offset(-8)
        }
        typeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(imageView.snp.bottom).offset(12)
            make.left.equalTo(8)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(typeLabel.snp.bottom).offset(4)
            make.left.equalTo(8)
            make.right.equalTo(-8)
        }
        addressLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
            make.left.equalTo(8)
            make.right.equalTo(-8)
        }
        lineView.snp.makeConstraints { (make) in
            make.top.equalTo(addressLabel.snp.bottom).offset(4)
            make.left.equalToSuperview().offset(8)
            make.right.equalToSuperview().offset(-8)
            make.height.equalTo(1)
        }
        reservedView.snp.makeConstraints { (make) in
            make.left.equalTo(8)
            make.top.equalTo(lineView.snp.bottom).offset(4)
        }
        ratingView.snp.makeConstraints { (make) in
            make.right.equalTo(-8)
            make.top.equalTo(lineView.snp.bottom).offset(4)
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .white
        layer.cornerRadius = 8
        layer.masksToBounds = true
        
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        
        titleLabel.textColor = .black
        titleLabel.font =  UIFont.systemFont(ofSize: 12, weight: .medium)
        
        typeLabel.textColor = .gray
        typeLabel.font = UIFont.systemFont(ofSize: 11)
        
        addressLabel.textColor = .black
        addressLabel.font = UIFont.systemFont(ofSize: 11)
        
        cashBackView.isHidden = true
        
        lineView.backgroundColor = .lightGray
    }
}
