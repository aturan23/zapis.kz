//
//  HorizontalListItemsTVCell.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class HorizontalListItemsTVCell: UITableViewCell {
    
    let viewController = HorizontalListItemsViewController(collectionViewLayout: HorizontalCVLayout())
    
    func set(models: [Firm]) {
        viewController.set(models: models)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    static func getCellHeight() -> CGFloat {
        return (screenSize.width - 32) * 9 / 22 + 32 + 4 + 28
    }
    
    static func getCellWidth() -> CGFloat {
        return (screenSize.width - 32) * 10 / 22 + 8
    }
}

extension HorizontalListItemsTVCell {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    private func addSubviews() {
        addSubview(viewController.view)
    }
    
    private func addConstraints() {
        viewController.view.translatesAutoresizingMaskIntoConstraints = false
        viewController.view.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        viewController.view.heightAnchor.constraint(
            greaterThanOrEqualToConstant: HorizontalListItemsTVCell.getCellHeight()
        ).isActive = true
    }
    
    private func stylizeViews() {
        backgroundColor = .clear
    }
}
