//
//  City.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

struct City: Codable {
    let id: Int
    let name, urlName: String
    let longitude, latitude: Double
}
