//
//  Dashboard.swift
//  zapis.kz
//
//  Created by User on 7/30/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

struct Dashboard: Codable {
    let recommendedFirms, popularFirms, recentlyAddedFirms, masters: [Firm]
    let cities: [City]
    let wrongServicePricePhones: [String]
}
