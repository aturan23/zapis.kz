//
//  Firm.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

struct Firm: Codable {
    let id: Int
    let name, address, type: String
    let avatarUrl: String?
    let workSchedule: String?
    let pictureUrl: String?
    let averageRating: Float?
    let prepaymentCashbackAmount: String?
    let todayReservationsCount: Int?
    let phoneNumbers: [String]?
    let pictures: [String]?
}
