//
//  DashboardTableSection.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

enum SectionType: String {
    case recommendedFirms   = "Рекомендуемое"
    case popularFirms       = "Популярные"
    case recentlyAddedFirms = "Новые"
    case masters            = "Мастера"
    case none               = ""
}

struct DashboardTableSection {
    let type: SectionType
    let rowList: [Firm]
}
