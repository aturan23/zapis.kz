//
//  DashboardViewController.swift
//  zapis.kz
//
//  Created by User on 7/30/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class DashboardViewController: UITableViewController, Alertable {
    
    private var viewModel: DashboardViewModel = DefaultDashboardViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(to: viewModel)
        viewModel.viewDidLoad()
    }
    
    // MARK: - Bind with view
    private func bind(to viewModel: DashboardViewModel) {
        viewModel.error.observe(on: self) { [unowned self] in self.showAlert(type: .error, $0)}
        viewModel.loading.observe(on: self) { LoadingView.show($0) }
        viewModel.list.observe(on: self) { [unowned self] in
            if $0.count > 0 {
                self.tableView.reloadData()
            }
        }
    }
}

// MARK: - TableView
extension DashboardViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.list.value.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: HorizontalListItemsTVCell = tableView.dequeueReusableCell(forIndexPath: indexPath)
        addChild(cell.viewController)
        cell.set(models: viewModel.list.value[indexPath.section].rowList)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SectionHeaderView()
        view.configure(type: viewModel.list.value[section].type)
        return view
    }
}

// MARK: - View init
extension DashboardViewController {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    
    private func addSubviews() {
        
    }
    
    private func addConstraints() {
        
    }
    
    private func stylizeViews() {
        view.backgroundColor = UIColor(named: Color.light)
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.register(HorizontalListItemsTVCell.self)
    }
}
