//
//  HorizontalListItemsViewController.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class HorizontalListItemsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    private var models = [Firm]()
    
    func set(models: [Firm]) {
        self.models = models
        collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stylizeViews()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: HorizontalListItemCVCell = collectionView.dequeueReusableCell(forIndexPath: indexPath)
        cell.set(model: models[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: HorizontalListItemsTVCell.getCellWidth(), height: HorizontalListItemsTVCell.getCellHeight())
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let viewModel = DefaultDetailViewModel(id: models[indexPath.row].id)
        let vc = DetailViewController(viewModel: viewModel)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension HorizontalListItemsViewController {
    private func stylizeViews() {
        view.backgroundColor = .clear
        collectionView.backgroundColor = UIColor(named: Color.light)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(cell: HorizontalListItemCVCell.self)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
    }
}
