//
//  DashboardViewModel.swift
//  zapis.kz
//
//  Created by User on 7/30/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

protocol DashboardViewModelInput {
    func viewDidLoad()
}

protocol DashboardViewModelOutput {
    var error: Observable<String> { get }
    var loading: Observable<Bool> { get }
    var list: Observable<[DashboardTableSection]> { get }
}

protocol DashboardViewModel: DashboardViewModelInput, DashboardViewModelOutput { }

final class DefaultDashboardViewModel: DashboardViewModel {
    
    let manager = ServiceProvider<DashboardProvider>()
    
    // MARK: - OUTPUT
    let loading: Observable<Bool> = Observable(false)
    let error: Observable<String> = Observable("")
    let list: Observable<[DashboardTableSection]> = Observable([])
    
    private func loadList() {
        self.loading.value = true
        manager.load(service: .list, type: Response<Dashboard>.self) { (result) in
            self.loading.value = false
            switch result {
            case .success(let response):
                self.list.value = self.populateList(with: response.data)
            case .failure(let error):
                self.error.value = error
            }
        }
    }
    
    private func populateList(with model: Dashboard) -> [DashboardTableSection] {
        let recommendSection = DashboardTableSection(type: .recommendedFirms, rowList: model.recommendedFirms)
        let popularSection = DashboardTableSection(type: .popularFirms, rowList: model.popularFirms)
        let recentSection = DashboardTableSection(type: .recentlyAddedFirms, rowList: model.recentlyAddedFirms)
        let mastersSection = DashboardTableSection(type: .masters, rowList: model.masters)
        return [recommendSection, popularSection, recentSection, mastersSection]
    }
}

// MARK: - INPUT
extension DefaultDashboardViewModel {
    
    func viewDidLoad() {
        loadList()
    }
}
