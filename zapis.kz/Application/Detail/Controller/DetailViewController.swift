//
//  DetailViewController.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class DetailViewController: UITableViewController, Alertable {
    
    var viewModel: DetailViewModel
    
    private var headerView = DetailTVHeader(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 360))
    
    // MARK: - Init
    init(viewModel: DetailViewModel) {
        self.viewModel = viewModel
        super.init(style: .plain)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        bind(to: viewModel)
        viewModel.viewDidLoad()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Bind with view
    private func bind(to viewModel: DetailViewModel) {
        viewModel.error.observe(on: self) { [unowned self] in self.showAlert(type: .error, $0)}
        viewModel.loading.observe(on: self) { LoadingView.show($0) }
        viewModel.model.observe(on: self) { [weak self] model in
            if let model = model {
                self?.headerView.configure(model: model)
                self?.navigationItem.title = model.firm.name
                self?.tableView.reloadData()
            }
        }
    }
}

// MARK: - Actions
extension DetailViewController: ButtonActionDelegate {
    func tapAction() {
        let vc = InMapViewController()
        vc.configure(location: viewModel.model.value!.location)
        navigationController?.pushViewController(vc, animated: true)
    }
}

// MARK: - TableView
extension DetailViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        viewModel.list.value.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let sectionModel = viewModel.list.value[indexPath.section]
        switch sectionModel.type {
        case .none:
            let cell = ServiceTVCell()
            cell.configure(model: sectionModel.rowList[indexPath.row])
            return cell
        default:
            let cell = MasterTVCell()
            cell.configure(model: sectionModel.rowList[indexPath.row])
            cell.accessoryType = .disclosureIndicator
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = SectionHeaderView()
        view.configure(type: viewModel.list.value[section].type)
        return view
    }
}

// MARK: - Stylize
extension DetailViewController {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    
    private func addSubviews() {
        
    }
    
    private func addConstraints() {
        
    }
    
    private func stylizeViews() {
        view.backgroundColor = UIColor(named: Color.light)
        
        headerView.actionDelegate = self
        
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.tableHeaderView = headerView
        tableView.register(ServiceTVCell.self)
    }
}
