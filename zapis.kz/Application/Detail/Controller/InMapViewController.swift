//
//  InMapViewController.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit
import YandexMapKit

class InMapViewController: UIViewController {
    
    private var mapView = YMKMapView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func configure(location: Location) {
        
        let center = YMKPoint(latitude: Double(location.centerY), longitude: Double(location.centerX))
        mapView.mapWindow.map.move(
            with: YMKCameraPosition(target: center, zoom: Float(location.zoom), azimuth: 0, tilt: 0),
        animationType: YMKAnimation(type: YMKAnimationType.smooth, duration: 0.3),
        cameraCallback: nil)
    }
}

// MARK: - Stylize
extension InMapViewController {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    
    private func addSubviews() {
        view.addSubview(mapView)
    }
    
    private func addConstraints() {
        mapView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    private func stylizeViews() {
        view.backgroundColor = .white
    }
}
