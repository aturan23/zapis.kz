//
//  DetailTVHeader.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit
import ImageSlideshow

class DetailTVHeader: UIView {
    
    var actionDelegate: ButtonActionDelegate?
    private var slideshowView = ImageSlideshow()
    private var titleLabel = UILabel()
    private var typeLabel = UILabel()
    private var mapButton = UIButton()
    private var addressLabel = UILabel()
    private var instaButton = UIButton()
    private var callButton = UIButton()
    private let lineView = UIView()
    private let reservedView = TitleImageView(size: .large)
    private let ratingView = TitleImageView(size: .large)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(model: Detail) {
        var pictureSource: [KingfisherSource] = []
        (model.firm.pictures ?? []).forEach { (pictureUrl) in
            pictureSource.append(KingfisherSource(url: pictureUrl.imageUrl!))
        }
        slideshowView.setImageInputs(pictureSource)
        
        titleLabel.text = model.firm.name
        typeLabel.text = model.firm.type
        addressLabel.text = model.firm.address.withoutHtml
        reservedView.configure(text: (model.firm.todayReservationsCount ?? 0).description, sfName: "person.2.fill", color: .purple)
        ratingView.configure(text: 5.description, sfName: "star.fill", color: .orange)
    }
    
    // MARK: - Actions
    @objc func tapInMap() {
        actionDelegate?.tapAction()
    }
}

// MARK: - Stylize
extension DetailTVHeader {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    
    private func addSubviews() {
        addSubviews([
            slideshowView,
            titleLabel,
            typeLabel,
            mapButton,
            addressLabel,
            instaButton,
            callButton,
            lineView,
            reservedView,
            ratingView
        ])
    }
    
    private func addConstraints() {
        slideshowView.snp.makeConstraints { (make) in
            make.left.right.top.equalToSuperview()
            make.height.equalTo(200)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(slideshowView.snp.bottom).offset(16)
            make.left.equalTo(16)
        }
        typeLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(12)
            make.left.equalTo(16)
            make.right.equalTo(-16)
        }
        mapButton.snp.makeConstraints { (make) in
            make.top.equalTo(typeLabel.snp.bottom).offset(12)
            make.left.equalTo(16)
            make.height.width.equalTo(30)
        }
        addressLabel.snp.makeConstraints { (make) in
            make.centerY.equalTo(mapButton.snp.centerY)
            make.left.equalTo(mapButton.snp.right).offset(8)
            make.right.equalTo(instaButton.snp.left).offset(-8)
        }
        callButton.snp.makeConstraints { (make) in
            make.top.equalTo(typeLabel.snp.bottom).offset(16)
            make.right.equalTo(-16)
            make.height.width.equalTo(30)
        }
        instaButton.snp.makeConstraints { (make) in
            make.centerY.equalTo(callButton.snp.centerY)
            make.right.equalTo(callButton.snp.left).offset(-8)
            make.height.width.equalTo(30)
        }
        lineView.snp.makeConstraints { (make) in
            make.top.equalTo(instaButton.snp.bottom).offset(8)
            make.left.equalToSuperview().offset(16)
            make.right.equalToSuperview().offset(-16)
            make.height.equalTo(1)
        }
        reservedView.snp.makeConstraints { (make) in
            make.top.equalTo(lineView.snp.bottom).offset(8)
            make.left.equalTo(16)
        }
        ratingView.snp.makeConstraints { (make) in
            make.top.equalTo(lineView.snp.bottom).offset(4)
            make.right.equalTo(-16)
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .white
        
        slideshowView.pageIndicatorPosition = .init(horizontal: .center, vertical: .bottom)
        slideshowView.contentScaleMode = .scaleAspectFill

        let pageControl = UIPageControl()
        pageControl.currentPageIndicatorTintColor = .lightGray
        pageControl.pageIndicatorTintColor = .black
        slideshowView.pageIndicator = pageControl
        slideshowView.activityIndicator = DefaultActivityIndicator()
        
        titleLabel.font = .systemFont(ofSize: 15)
        
        typeLabel.font = .systemFont(ofSize: 13)
        typeLabel.textColor = .gray
        
        mapButton.setImage(UIImage(systemName: "safari"), for: .normal)
        mapButton.backgroundColor = UIColor(named: Color.light)
        mapButton.tintColor = .gray
        mapButton.layer.cornerRadius = 8
        mapButton.addTarget(self, action: #selector(tapInMap), for: .touchUpInside)
        
        instaButton.setImage(UIImage(systemName: "bookmark"), for: .normal)
        instaButton.backgroundColor = UIColor(named: Color.light)
        instaButton.tintColor = .gray
        instaButton.layer.cornerRadius = 8
        
        callButton.setImage(UIImage(systemName: "phone"), for: .normal)
        callButton.backgroundColor = UIColor(named: Color.light)
        callButton.tintColor = .gray
        callButton.layer.cornerRadius = 8
        
        addressLabel.font = .systemFont(ofSize: 14)
        addressLabel.textColor = .gray
        
        lineView.backgroundColor = .gray
    }
}
