//
//  MasterTVCell.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class MasterTVCell: UITableViewCell {
    
    private var profileView = UIImageView()
    private var titleLabel = UILabel()
    private var subTitleLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(model: Codable) {
        if let model = model as? Master {
            titleLabel.text = model.name
            subTitleLabel.text = model.profession
            if let url = model.avatarUrl {
                profileView.kf.setImage(with: url.imageUrl)
            }
        }
    }
}

extension MasterTVCell {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    private func addSubviews() {
        addSubviews([profileView, titleLabel, subTitleLabel])
    }
    
    private func addConstraints() {
        profileView.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.left.equalTo(12)
            make.bottom.equalTo(-12)
            make.height.width.equalTo(40)
        }
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.left.equalTo(profileView.snp.right).offset(8)
        }
        subTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.left.equalTo(profileView.snp.right).offset(8)
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .white
        
        profileView.layer.cornerRadius = 20
        profileView.layer.masksToBounds = true
        
        titleLabel.font = .systemFont(ofSize: 15)
        titleLabel.textColor = .black
        
        subTitleLabel.font = .systemFont(ofSize: 12)
        subTitleLabel.textColor = .gray
    }
}
