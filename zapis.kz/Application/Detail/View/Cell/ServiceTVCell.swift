//
//  ServiceTVCell.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class ServiceTVCell: UITableViewCell {
    
    private var titleLabel = UILabel()
    private var subTitleLabel = UILabel()
    private var addButton = UIButton()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(model: Codable) {
        if let model = model as? Service {
            titleLabel.text = model.name
            subTitleLabel.text = "\(model.duration) мин * \(model.priceMax) - \(model.price)"
        }
    }
}

extension ServiceTVCell {
    private func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
    private func addSubviews() {
        addSubviews([titleLabel, subTitleLabel, addButton])
    }
    
    private func addConstraints() {
        titleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.left.equalTo(16)
        }
        subTitleLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.left.equalTo(16)
            make.bottom.equalTo(-12)
        }
        addButton.snp.makeConstraints { (make) in
            make.right.equalTo(-16)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(40)
        }
    }
    
    private func stylizeViews() {
        backgroundColor = .white
        
        titleLabel.font = .systemFont(ofSize: 15)
        titleLabel.textColor = .black
        
        subTitleLabel.font = .systemFont(ofSize: 12)
        subTitleLabel.textColor = .gray
        
        addButton.setImage(UIImage(systemName: "plus.square"), for: .normal)
        addButton.tintColor = .black
    }
}
