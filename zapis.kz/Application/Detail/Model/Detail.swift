//
//  Detail.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

struct Detail: Codable {
    let firm: Firm
    let masters: [Master]
    let services: [Service]
    let categories: [Category]
    let location: Location
}

struct Category: Codable {
    let id: Int
    let name: String
}

// MARK: - Location
struct Location: Codable {
    let type: String
    let markerX, markerY, centerX, centerY: Float
    let zoom: Int
}

// MARK: - Master
struct Master: Codable {
    let id: Int
    let name: String
    let surname: String?
    let profession: String
    let avatarUrl: String?
    let rating: Float?
    let experience: String?
}

// MARK: - Service
struct Service: Codable {
    let id: Int
    let name: String
    let description: String?
    let price, priceMax, duration: Int
    let express: Int?
    let priceStr: String
    let prepaymentAmount: Int?
}
