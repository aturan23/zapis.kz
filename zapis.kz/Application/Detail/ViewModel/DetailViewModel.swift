//
//  DetailViewModel.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

protocol DetailViewModelInput {
    func viewDidLoad()
}

protocol DetailViewModelOutput {
    var error: Observable<String> { get }
    var loading: Observable<Bool> { get }
    var model: Observable<Detail?> { get }
    var list: Observable<[DetailTableSection]> { get }
}

protocol DetailViewModel: DetailViewModelInput, DetailViewModelOutput { }

final class DefaultDetailViewModel: DetailViewModel {
    
    let manager = ServiceProvider<DashboardProvider>()
    let id: Int
    
    // MARK: - OUTPUT
    let loading: Observable<Bool> = Observable(false)
    let error: Observable<String> = Observable("")
    let model: Observable<Detail?> = Observable(nil)
    let list: Observable<[DetailTableSection]> = Observable([])
    
    init(id: Int) {
        self.id = id
    }
    
    private func loadDetail() {
        self.loading.value = true
        manager.load(service: .detail(id: id), type: Response<Detail>.self) { (result) in
            self.loading.value = false
            switch result {
            case .success(let response):
                self.model.value = response.data
                self.list.value = self.populateList(with: response.data)
            case .failure(let error):
                self.error.value = error
            }
        }
    }
    
    private func populateList(with model: Detail) -> [DetailTableSection] {
        let recommendSection = DetailTableSection(type: .none, rowList: model.services)
        let mastersSection = DetailTableSection(type: .masters, rowList: model.masters)
        return [recommendSection, mastersSection]
    }
}

// MARK: - INPUT
extension DefaultDetailViewModel {
    
    func viewDidLoad() {
        loadDetail()
    }
}
