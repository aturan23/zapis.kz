//
//  Alertable.swift
//  zapis.kz
//
//  Created by User on 7/30/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

//  MARK: - message alert type
enum AlertMessageType: String {
    case error = "Ошибка"
    case warning = "Внимание"
    case success = "Успешно"
    case none = ""
}

public protocol Alertable {}

extension Alertable where Self: UIViewController {
    
    func showAlert(type: AlertMessageType, _ message: String, preferredStyle: UIAlertController.Style = .alert, completion: (() -> Void)? = nil) {
        guard !message.isEmpty else { return }
        let alert = UIAlertController(title: type.rawValue, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: completion)
    }
}
