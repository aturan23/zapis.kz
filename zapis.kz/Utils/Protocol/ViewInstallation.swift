//
//  ViewInstallation.swift
//  zapis.kz
//
//  Created by User on 7/30/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

protocol ViewInstallation {
    func addSubviews()
    func addConstraints()
    func stylizeViews()
}

extension ViewInstallation {
    func setupViews() {
        addSubviews()
        stylizeViews()
        addConstraints()
    }
}
