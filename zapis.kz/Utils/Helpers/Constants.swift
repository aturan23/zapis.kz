//
//  Constants.swift
//  zapis.kz
//
//  Created by User on 7/30/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

typealias VoidCompletion = (() -> Void)

struct AppConstants {
   static let window = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
    static let statusBarHeight = window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
    static let screenHeight = UIScreen.main.bounds.height
    static let screenWidth = UIScreen.main.bounds.width
    static let navBarHeight = UINavigationController().navigationBar.bounds.height
    static let topHeight = navBarHeight + statusBarHeight
    static let apiKey = "a8e5b443-3414-4b0f-a240-d706b713d93a"
}

let screenSize = UIScreen.main.bounds.size

struct Color {
    static let light = "Light"
}
