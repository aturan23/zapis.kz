//
//  UIView+Extensions.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

//  MARK: - UIViewController
extension UIViewController{
    func inNavigation() -> UIViewController {
        return UINavigationController(rootViewController: self)
    }
}

//  MARK: - UIView
extension UIView {
    func addShadow(_ radius: CGFloat = 20.0) {
        layer.cornerRadius = radius
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        layer.shadowRadius = 3.0
        layer.shadowOpacity = 0.7
    }
    func addSubviews(_ views : [UIView]) -> Void {
        views.forEach { (view) in
            self.addSubview(view)
        }
    }
    func addShadow(with color: UIColor, radius: CGFloat) {
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = radius
    }
}
