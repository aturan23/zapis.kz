//
//  String+Extension.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

extension String {
    
    var imageUrl: URL? {
        return URL(string: "http://zp.jgroup.kz" + self)
    }
    
    var withoutHtml: String {
        return self.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
}
