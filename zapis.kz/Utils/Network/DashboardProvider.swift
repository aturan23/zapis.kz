//
//  DashboardProvider.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation
import Alamofire

enum DashboardProvider: URLRequestBuilder {
    case list
    case detail(id: Int)
    
    var path: String {
        switch self {
        case .list:
            return "screen/home"
        case .detail(let id):
            return "firms/" + id.description
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    var parameters: Parameters? {
        return nil
    }
    
    var method: HTTPMethod {
        return .get
    }
}
