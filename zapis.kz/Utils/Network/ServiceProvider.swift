//
//  ServiceProvider.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation
import Alamofire

class ServiceProvider<T: URLRequestBuilder> {
    
    func load<U: Codable>(service: T, type: U.Type, completion: @escaping (Result<U>) -> Void)  {
        guard let urlRequest = service.urlRequest else { return }
        AF.request(urlRequest).validate().responseDecodable(of: U.self) { (response) in
            switch response.result {
            case .success(let result):
                completion(.success(result))
            case .failure(let error):
                completion(.failure(error.localizedDescription))
            }
        }
    }
}
