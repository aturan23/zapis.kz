//
//  Response.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

struct Response<T: Codable>: Codable {
    let message: String?
    let data: T
}
