//
//  Result.swift
//  zapis.kz
//
//  Created by User on 7/30/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import Foundation

public enum Result<T: Codable> {
    case failure(String)
    case success(T)
}
