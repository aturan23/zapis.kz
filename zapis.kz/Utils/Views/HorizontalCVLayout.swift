//
//  HorizontalCVLayout.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

class HorizontalCVLayout: UICollectionViewFlowLayout {
    override init() {
        super.init()
        scrollDirection = .horizontal
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
