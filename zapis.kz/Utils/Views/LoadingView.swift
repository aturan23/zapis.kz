//
//  LoadingView.swift
//  zapis.kz
//
//  Created by User on 7/31/20.
//  Copyright © 2020 aturan23. All rights reserved.
//

import UIKit

public class LoadingView {

    internal static var spinner: UIActivityIndicatorView?
    
    public static func show(_ show: Bool) {
        if show {
            DispatchQueue.main.async {
                NotificationCenter.default.addObserver(self, selector: #selector(update), name: UIDevice.orientationDidChangeNotification, object: nil)
                if spinner == nil, let window = AppConstants.window {
                    let frame = UIScreen.main.bounds
                    let spinner = UIActivityIndicatorView(frame: frame)
                    spinner.backgroundColor = UIColor.black.withAlphaComponent(0.2)
                    spinner.style = .large
                    window.addSubview(spinner)

                    spinner.startAnimating()
                    self.spinner = spinner
                }
            }
        } else {
            DispatchQueue.main.async {
                guard let spinner = spinner else { return }
                spinner.stopAnimating()
                spinner.removeFromSuperview()
                self.spinner = nil
            }
        }
    }
    
    @objc public static func update() {
        DispatchQueue.main.async {
            if spinner != nil {
                show(false)
                show(true)
            }
        }
    }
}
